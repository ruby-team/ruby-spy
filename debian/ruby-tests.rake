#require 'gem2deb/rake/spectask'
require 'gem2deb/rake/testtask'

#RSpec::Core::RakeTask.new(:spec) do |spec|
#  spec.pattern = './spec/**/*_spec.rb'
#end

Gem2Deb::Rake::TestTask.new do |t|
  t.libs << "test"
  t.test_files = FileList['test/**/test*.rb']
  t.verbose = true
end

#task :default => :spec
task :default => :test
